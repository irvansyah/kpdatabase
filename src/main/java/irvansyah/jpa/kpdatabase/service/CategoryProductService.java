package irvansyah.jpa.kpdatabase.service;

import irvansyah.jpa.kpdatabase.dto.CategoryProductDto;
import irvansyah.jpa.kpdatabase.model.CategoryProduct;
import irvansyah.jpa.kpdatabase.repository.CategoryProductRepo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CategoryProductService {

    @Autowired
    CategoryProductRepo categoryProductRepo;

    public ResponseEntity create(CategoryProductDto request){
        try {
            CategoryProduct categoryProduct = new CategoryProduct();
            BeanUtils.copyProperties(request,categoryProduct);
            categoryProduct.setCreatedAt(new Timestamp(System.currentTimeMillis()));
            categoryProduct.setUpdatedAt(null);
            categoryProductRepo.save(categoryProduct);
            return ResponseEntity.ok().body(categoryProduct);
        }catch (Exception e){
            return ResponseEntity.notFound().build();
        }

    }

    public ResponseEntity getData(){
        List<CategoryProduct> categoryProducts = categoryProductRepo
                .findAll();
        List<CategoryProductDto> categoryProductDtos = new ArrayList<>();
        for (CategoryProduct categoryProduct : categoryProducts){
            CategoryProductDto dto = new CategoryProductDto();
            BeanUtils.copyProperties(categoryProduct, dto);
            categoryProductDtos.add(dto);
            return ResponseEntity.status(HttpStatus.FOUND).body(categoryProducts);
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    public ResponseEntity getIdProduct(long productId){
        Optional<CategoryProduct> categoryProduct = categoryProductRepo.findById(productId);
        List<CategoryProductDto> balikan = new ArrayList<>();
        if (categoryProduct.isPresent()){
            CategoryProductDto productDto = new CategoryProductDto();
            BeanUtils.copyProperties(categoryProduct, productDto);
            balikan.add(productDto);
            return ResponseEntity.status(HttpStatus.OK).build();

        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

