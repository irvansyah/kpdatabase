package irvansyah.jpa.kpdatabase.service;


import irvansyah.jpa.kpdatabase.dto.CategoriesDto;
import irvansyah.jpa.kpdatabase.model.Categories;
import irvansyah.jpa.kpdatabase.repository.CategoriesRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import javax.persistence.EntityManager;
import java.util.*;

@Service
public class CategoriesService {

    private static final Logger logger =    LoggerFactory.getLogger(CategoriesService.class.getName());


    @Autowired
    CategoriesRepository categoriesRepository;

    @Autowired
    EntityManager entityManager;

    public ResponseEntity createData(CategoriesDto request) {
        LinkedHashMap<String, Object> res = new LinkedHashMap<>();
        try {
//            logger.debug("Mandala Finance");
//            logger.info("test");
            Categories categories = new Categories();
            BeanUtils.copyProperties(request, categories);
            categories.setCreatedAt(new Date());
            categories.setUpdatedAt(null);
            categoriesRepository.save(categories);
            res.put("status", "200");
            res.put("message", "data berhasil di input");
            res.put("data", categories);
            return ResponseEntity.ok().body(res);
        } catch (Exception e) {
            res.put("status", false);
            res.put("message", e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }

    public ResponseEntity getData() {
        LinkedHashMap<String, Object> res = new LinkedHashMap<>();
        try {
//            logger.debug("Mandala Finance");
//            logger.info("test");
            List<Categories> categories = categoriesRepository.findAll();
            List<CategoriesDto> returner = new ArrayList<>();
            for (Categories categories1 : categories) {
                CategoriesDto categoriesDto = new CategoriesDto();
                BeanUtils.copyProperties(categories1, categoriesDto);
                returner.add(categoriesDto);

            }
            res.put("status", "200");
            res.put("message", "data ditemukan");
            res.put("data", categories);
//            logger.info("selesai", categories);
            return ResponseEntity.ok().body(res);

        } catch (Exception e) {
            res.put("status", false);
            res.put("message", e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }

    public ResponseEntity getById(long Id) {

        LinkedHashMap<String, Object> res = new LinkedHashMap<>();
        try {
            logger.debug("log started");
            Optional<Categories> categories = categoriesRepository.findById(Id);
            Categories categories1 = new Categories();
            BeanUtils.copyProperties(categories.get(), categories1);
            res.put("status", "200");
            res.put("message", "data ditemukan");
            res.put("data", categories1);
//

            return ResponseEntity.ok().body(res);
        } catch (Exception e) {
            logger.warn("WARNING");
            logger.error("{}", e.getMessage());


//            res.put("status", false);
//            res.put("message", e.getMessage());

            return ResponseEntity.badRequest().body(res);
        }

    }

    public ResponseEntity updatedata(CategoriesDto request, @RequestParam Long Id) {
        Optional<Categories> existing = categoriesRepository.findById(Id);       if (existing.isPresent()) {
            Categories categories1 = new Categories();
            BeanUtils.copyProperties(request, categories1);
            categories1.setId(Id);
            categories1.setCreatedAt(existing.get().getCreatedAt());
            categories1.setUpdatedAt(new Date());
            categoriesRepository.save(categories1);
            return ResponseEntity.ok().body(categories1);
        }

        return ResponseEntity.notFound().build();
    }

    public boolean deleteData(Long Id)  throws Exception {
    Optional<Categories> categories = categoriesRepository.findById(Id);

    if (categories.isPresent()){
        categoriesRepository.delete(categories.get());
        return true;
    }else {
throw new Exception("id user not found");
    }
    }

}

