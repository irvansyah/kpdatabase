package irvansyah.jpa.kpdatabase.service;

import irvansyah.jpa.kpdatabase.dto.CategoryDto;
import irvansyah.jpa.kpdatabase.model.Category;
import irvansyah.jpa.kpdatabase.repository.CategoryRepo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CategoryService {

    @Autowired
    CategoryRepo categoryRepo;

    public ResponseEntity create(CategoryDto request) {
        try {
            Category category = new Category();
            BeanUtils.copyProperties(request, category);
            category.setCreatedAt(new Timestamp(System.currentTimeMillis()));
            category.setUpdatedAt(null);
            categoryRepo.save(category);
            return ResponseEntity.status(HttpStatus.CREATED).body(category);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    public ResponseEntity getData() {
        List<Category> categories = categoryRepo.findAll();
        List<CategoryDto> returner = new ArrayList<>();
        for (Category category : categories) {
            CategoryDto categoryDto = new CategoryDto();
            BeanUtils.copyProperties(category, categoryDto);
            returner.add(categoryDto);
            return ResponseEntity.status(HttpStatus.OK).body(categories);
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    public ResponseEntity getById(long Id) {
//        Optional<Category> category = categoryRepo.findById(Id);
//        Category category1 = new Category();
//        BeanUtils.copyProperties(category.get(),category1);
//        return ResponseEntity.ok().body(category);
        Optional<Category> category = categoryRepo.findById(Id);
        List<CategoryDto> returner = new ArrayList<>();
        if (category.isPresent()) {
            CategoryDto categoryDto = new CategoryDto();
            BeanUtils.copyProperties(category, categoryDto);
            returner.add(categoryDto);
            return ResponseEntity.status(HttpStatus.OK).body(category);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    public ResponseEntity updateData(CategoryDto request, long Id) {
        Optional<Category> existing = categoryRepo.findById(Id);
        if (existing.isPresent()) {
            Category category = new Category();
            BeanUtils.copyProperties(request, category);
            category.setId(Id);
            category.setCreatedAt(existing.get().getCreatedAt());
            category.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
            categoryRepo.save(category);
            return ResponseEntity.ok().body(category);

        }
        return ResponseEntity.notFound().build();
    }

    public ResponseEntity<Category> partial(long Id, CategoryDto request) {
        Optional<Category> category = categoryRepo.findById(Id);
        if (!category.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        Category category1 = category.get();
        category1.setId(Id);
        category1.setCreatedAt(category1.getCreatedAt());
        category1.setUpdatedAt(new Timestamp(System.currentTimeMillis()));

        if (request.getName() != null && !request.getName().isEmpty()) {
            category1.setName(request.getName());
        }
        if (request.getSlug() != null && !request.getSlug().isEmpty()) {
            category1.setSlug(request.getSlug());
        }
        categoryRepo.save(category1);
        return ResponseEntity.status(HttpStatus.OK).body(category1);
    }

    public boolean deletedata(Long Id) throws Exception {
        Optional<Category> category = categoryRepo.findById(Id);

        if (category.isPresent()) {
            categoryRepo.delete(category.get());
            return true;
        } else {
            throw new Exception("id not found");
        }
    }
}




