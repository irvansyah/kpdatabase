package irvansyah.jpa.kpdatabase.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import irvansyah.jpa.kpdatabase.dto.CategoryProductDto;
import irvansyah.jpa.kpdatabase.service.CategoryProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/category product")
@Api("api category product")
public class CategoryProductController {

    @Autowired
    CategoryProductService categoryProductService;

    @PostMapping(path = "/get",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("untuk mengambil semua data")
    public ResponseEntity createData(@RequestBody CategoryProductDto request){

        return categoryProductService.create(request);
    }

    @GetMapping(path = "/getData", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("ambil sema data")
    public ResponseEntity get (){

        return ResponseEntity.ok(categoryProductService.getData());
    }

    @GetMapping(path = "/get/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("get by id")
    public ResponseEntity getById(@RequestParam long productId){
        return categoryProductService.getIdProduct(productId);
    }
}
