package irvansyah.jpa.kpdatabase.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import irvansyah.jpa.kpdatabase.dto.CategoryDto;
import irvansyah.jpa.kpdatabase.model.Category;
import irvansyah.jpa.kpdatabase.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("/v1/category")
@Api(description = "api untuk tabel category")
public class CategoryController {

    @Autowired
    CategoryService categoryService;


    @PostMapping(path = "/input",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("input data")
    public ResponseEntity in (@RequestBody CategoryDto request){

        return categoryService.create(request);
    }

    @GetMapping(path = "/getdata",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("mengambil data")
    public ResponseEntity get(){

        return categoryService.getData();
    }
    @GetMapping(path = "/get/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("get by id")
    public ResponseEntity getId(@RequestParam long Id){

        return categoryService.getById(Id);
    }

    @PutMapping(path = "/update",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("update")
    public ResponseEntity update(@RequestBody CategoryDto request,@RequestParam long Id){

        return categoryService.updateData(request, Id);
    }

    @PatchMapping(path = "/update/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("update patrial")
     ResponseEntity <Category> updatepartial(@Valid @PathVariable("id") long Id, @RequestBody CategoryDto request){

        return categoryService.partial(Id, request);
    }
    @DeleteMapping(value = "")
    @ApiOperation("delete data")
            ResponseEntity delete(@RequestParam(name = "Id") Long Id){
        try {
            return ResponseEntity.ok(categoryService.deletedata(Id));
        }catch (Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }

    }

}
