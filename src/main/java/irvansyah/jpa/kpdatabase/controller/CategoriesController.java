package irvansyah.jpa.kpdatabase.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import irvansyah.jpa.kpdatabase.dto.CategoriesDto;
import irvansyah.jpa.kpdatabase.service.CategoriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/v1/Categories")
@Api(description = "api untuk table categories")
public class CategoriesController {




    @Autowired
    CategoriesService categoriesService;


    @PostMapping(path = "/create",consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("api for create Categories" )
    ResponseEntity create(@RequestBody CategoriesDto request){

        return categoriesService.createData(request);
    }
    @GetMapping(path = "/get data", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("api for get all data")
    ResponseEntity getData(){

        return categoriesService.getData();
    }
    @GetMapping(path = "/get/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("api fot get by id")
    ResponseEntity getById(@RequestParam long Id){
        return categoriesService.getById(Id);
    }

    @PutMapping(path = "/update",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("api for update data")
    ResponseEntity update(@RequestParam long Id, @Valid @RequestBody CategoriesDto request){

        return categoriesService.updatedata(request,Id);

    }
    @DeleteMapping(value =" ")
    @ApiOperation("api untuk delete")
    ResponseEntity delete (@RequestParam (name = "Id") Long Id){
    try {
        return ResponseEntity.ok(categoriesService.deleteData(Id));
    } catch (Exception e){

        return ResponseEntity.badRequest().body(e.getMessage());
     }
    }

}
