package irvansyah.jpa.kpdatabase.repository;

import irvansyah.jpa.kpdatabase.model.Category;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CategoryRepo extends CrudRepository<Category,Long> {

    @Query(value = "select * from category",nativeQuery = true)
    List<Category> findAll();
}
