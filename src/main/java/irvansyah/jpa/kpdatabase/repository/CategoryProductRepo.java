package irvansyah.jpa.kpdatabase.repository;

import irvansyah.jpa.kpdatabase.model.CategoryProduct;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface CategoryProductRepo extends CrudRepository<CategoryProduct,Long> {

    @Query(value = "select * from category_product",nativeQuery = true)
    List <CategoryProduct> findAll();
}
