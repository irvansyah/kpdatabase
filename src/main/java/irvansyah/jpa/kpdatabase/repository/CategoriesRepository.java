package irvansyah.jpa.kpdatabase.repository;


import irvansyah.jpa.kpdatabase.model.Categories;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoriesRepository extends CrudRepository<Categories, Long> {

   @Query(value = "select * from categories",nativeQuery = true)
    List<Categories> findAll();

}
