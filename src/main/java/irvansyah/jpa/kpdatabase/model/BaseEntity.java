package irvansyah.jpa.kpdatabase.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
@SuperBuilder
@NoArgsConstructor
public class BaseEntity {
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "Created_at", length = 19)
    private Date created_Date;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "Updated_at", length = 19)
    private Date updated_Date;

    @Temporal(TemporalType.DATE)
    @Column(name = "Created_Date", length = 19)
    private Date createdDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "Updated_Date", length = 19)
    private Date updatedDate;
}
