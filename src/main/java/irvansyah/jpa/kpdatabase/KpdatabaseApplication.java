package irvansyah.jpa.kpdatabase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KpdatabaseApplication {

	public static void main(String[] args) {

		SpringApplication.run(KpdatabaseApplication.class, args);
	}

}
